<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController; 
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\RewardController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Login
    Route::get('/login',       [AuthController::class, 'getlogin']);
    Route::post('/login/post', [AuthController::class, 'login']);
// Login


// Sessions Exists
Route::group(['middleware' => 'sessions'], function () { 

        Route::post('logout/post',                                                  [AuthController::class,'logout']); 

        Route::get('/',                                                             [HomeController::class,'index']);
        Route::get('/home',                                                         [HomeController::class,'index']); 
 
        Route::get('/orders', [HomeController::class,'index']);  
        Route::get('/orders/fetch', [HomeController::class,'fetch']);  
        Route::get('/orders/fetchReward', [HomeController::class,'fetchReward']);  
        Route::get('/orders/fetchEditDelete', [HomeController::class,'fetchEditDelete']);  
        Route::post('/orders/create', [HomeController::class,'create']);  
        Route::post('/orders/update', [HomeController::class,'update']);  
        Route::post('/orders/delete', [HomeController::class,'delete']);  
        Route::post('/orders/checkout', [HomeController::class,'checkout']);  

        Route::get('/rewards', [RewardController::class,'index']);    
        Route::get('/rewards/fetchEditDelete', [RewardController::class,'fetchEditDelete']);    
        Route::post('/rewards/update', [RewardController::class,'update']);    
        Route::get('/rewards/findExpired', [RewardController::class,'findExpired']);    

        Route::get('/payments', [PaymentController::class,'index']);    

        Route::get('/users', [UserController::class,'index']);  
        Route::get('/users/fetch', [UserController::class,'fetch']);  
        Route::get('/users/fetchEditDelete', [UserController::class,'fetchEditDelete']);  
        Route::post('/users/create', [UserController::class,'create']);  
        Route::post('/users/update', [UserController::class,'update']);  
        Route::post('/users/delete', [UserController::class,'delete']);  

    
});
// Sessions Exists