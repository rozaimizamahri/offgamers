Framework
1. Laravel 8 
2. Requirement 
   - Apache
   - MySQL
   - PHP 7.4 ++

Cloning Repo 
1. Clone repo into xampp htdocs directory 
2. Open project using visual studio code

Enable Laravel Dependencies
3. Run command in project directory 
   - cp .env.example .env
   - composer update
   - php artisan key:generate
   - update .env file
   - npm install & npm run dev (optional)
   ENV file
    - Database name : offgamers
    - username and password using default xampp setting

Database Creation
3. Run command to create database and seed the data of user
   - php artisan migrate:fresh
   - php artisan db:seed UserSeeder

Access System
1. Run this link using browser http://localhost/offgamers
1. There are two (2) users to login
   - Micheal
   - Sarah
3. Login into http://localhost/offgamers
   - email : micheal@mail.com password : micheal123
   - email : sarah@mail.com password : sarah123

Workflow
1. Create new orders
2. Once order successfully created, you can proceed with checkout/payment
   - system will check points collected from previous purchase / sales order completed, if empty then return 0
   - first time order creation for that user will set to 0 point
3. Once payment done for that user, system will activate point and ready to be used in the next order for that specific user












To run scheduler to check expired reward afer 1 year (365 days and above)

Run this link using using internet browser 
- http://localhost/offgamers/rewards/findExpired

Run using command in laravel app
- php artisan command:rewardexpired